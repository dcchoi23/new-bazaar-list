import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { User } from '../user.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit, OnDestroy {
  signUpForm: FormGroup;
  isLoading: boolean;

  private authServiceSub: Subscription;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.authServiceSub = this.userService.getAuthStatusListener().subscribe(() => this.isLoading = false);

    this.signUpForm = new FormGroup({
      firstName: new FormControl(null, { validators: [Validators.required, Validators.minLength(2), Validators.maxLength(25)] }),
      lastName: new FormControl(null, { validators: [Validators.required, Validators.minLength(2), Validators.maxLength(25)] }),
      email: new FormControl(null, { validators: [Validators.required, Validators.minLength(5), Validators.maxLength(25)] }),
      password: new FormControl(null, { validators: [Validators.required, Validators.minLength(8), Validators.maxLength(15)] }),
      confirmPassword: new FormControl(null, { validators: [Validators.required, Validators.minLength(8), Validators.maxLength(15)] })
    });
  }

  saveUser() {
    if (this.signUpForm.invalid) {
      return;
    }

    const newUser: User = {
      firstName: this.signUpForm.value.firstName,
      lastName: this.signUpForm.value.lastName,
      email: this.signUpForm.value.email,
      password: this.signUpForm.value.password
    };

    this.userService.createUser(newUser);
  }

  ngOnDestroy() {
    this.authServiceSub.unsubscribe();
  }
}
