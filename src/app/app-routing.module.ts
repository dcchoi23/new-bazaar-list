import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TxtbkAdListComponent } from './textbooks/txtbk-ad-list/txtbk-ad-list.component';
import { TxtbkAdCreateComponent } from './textbooks/txtbk-ad-create/txtbk-ad-create.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: '', component: TxtbkAdListComponent },
  // Create and edit ad URL routes only available to authenticated users, otherwise redirect unauthenticated users to the login page
  { path: 'createAd', component: TxtbkAdCreateComponent, canActivate: [AuthGuard] },
  { path: 'edit/:postId', component: TxtbkAdCreateComponent, canActivate: [AuthGuard] },
  { path: 'user', loadChildren: './user.module#UserModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
