import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private authListenerSubscription: Subscription;
  isUserAuthenticated = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.isUserAuthenticated = this.userService.getIsAuth();
    this.authListenerSubscription = this.userService.getAuthStatusListener()
      .subscribe(authenticationStatus => {
        this.isUserAuthenticated = authenticationStatus;
      });
  }

  onLogout() {
    this.userService.logout();
  }

  ngOnDestroy() {
    this.authListenerSubscription.unsubscribe();
  }
}
