import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'new-bazaar-list';

  constructor(private userService: UserService) {}

  // Kick off automatic authentication of users assuming JWT token exists in local storage & is not expired
  ngOnInit() {
    this.userService.autoAuthUsers();
  }
}
