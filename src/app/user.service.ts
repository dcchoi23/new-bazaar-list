import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { User } from './user.model';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private SERVER_URL = environment.apiUrl + '/user';
  private tokenTimer: any;
  private token: string;
  private isAuthenticated = false;
  private userId: string;
  private authenticationStatusListener = new Subject<boolean>();

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient, private router: Router) { }

  getToken() {
    return this.token;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getAuthStatusListener() {
    return this.authenticationStatusListener.asObservable();
  }

  getUserId() {
    return this.userId;
  }

  createUser(user: any) {
    return this.http.post(this.SERVER_URL + '/signup', JSON.stringify(user), this.httpOptions).subscribe(() => {
      this.router.navigate(['/']);
    }, error => {
      // If user creation unsuccessful, sent a false boolean to the rest of the listeners
      this.authenticationStatusListener.next(false);
    });
  }

  login(email: string, password: string) {
    const authUser: User = { email: email, password: password };
    return this.http
      .post<{ token: string, expiresIn: number, userId: string }>(this.SERVER_URL + '/login', authUser)
      .subscribe(response => {
        this.token = response.token;
        if (this.token) {
          this.userId = response.userId;
          this._setAuthTimer(response.expiresIn);

          // If authenticated, push the true status to other components listening for the status
          this.authenticationStatusListener.next(true);

          // Determine when the JWT token expires relative to the current time
          const currentDate = new Date();
          const expirationDate = new Date(currentDate.getTime() + response.expiresIn * 1000);
          this._saveAuthToken(this.token, expirationDate, this.userId);

          this.router.navigate(['/']);
        }
      }, error => {
        this.authenticationStatusListener.next(false);
      });
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.userId = null;

    // Inform for all listeners (subscribers) that the current user is not authenticated anymore
    this.authenticationStatusListener.next(false);

    // Restart timer when user logins again
    clearTimeout(this.tokenTimer);

    this._clearAuthToken();

    this.router.navigate(['/']);
  }

  // Method to check if JWT token already exists in local storage and is valid (not expired)
  autoAuthUsers() {
    const authData = this._getAuthToken();

    // If there is no JWT auth token, cannot log user in automatically
    if (!authData) {
      return;
    }

    const currentDate = new Date();

    // Check if the expirary time of the token is past the current time
    const expiresIn = authData.expirationDate.getTime() - currentDate.getTime();

    // Expirary time of token is past the current time
    if (expiresIn > 0) {
      this.token = authData.token;
      this.isAuthenticated = true;
      this.userId = authData.userId;
      this.authenticationStatusListener.next(true);

      // Set the timer for when the token expires and will automatically log user out of app
      this._setAuthTimer(expiresIn / 1000);
    }
  }

  private _setAuthTimer(duration: number) {
    // After the JWT token expires, log user out of app by clearing auth token, informing listeners and navigating to homepage
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
    this.isAuthenticated = true;
  }

  private _saveAuthToken(token: string, expirationDate: Date, userId: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
    localStorage.setItem('userId', userId);
  }

  private _clearAuthToken() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
    localStorage.removeItem('userId');
  }

  // Get JWT token from local storage if it exists
  private _getAuthToken() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    const userId = localStorage.getItem('userId');
    if (!token || !expirationDate) {
      return;
    }
    return { token: token, expirationDate: new Date(expirationDate), userId: userId };
  }
}
