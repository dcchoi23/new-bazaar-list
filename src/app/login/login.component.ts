import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  isLoading = false;
  private authServiceSub: Subscription;

  constructor(public userService: UserService) { }

  ngOnInit() {

    this.authServiceSub = this.userService.getAuthStatusListener().subscribe(() => this.isLoading = false);

    this.loginForm = new FormGroup({
      email: new FormControl(null, {validators: [Validators.required, Validators.minLength(5), Validators.maxLength(25)]}),
      password: new FormControl(null, {validators: [Validators.required, Validators.minLength(8), Validators.maxLength(15)]})
    });
  }

  onlogin() {
    if (this.loginForm.invalid) {
      return;
    }
    this.isLoading = true;
    this.userService.login(this.loginForm.value.email, this.loginForm.value.password);
  }

  ngOnDestroy() {
    this.authServiceSub.unsubscribe();
  }

}
