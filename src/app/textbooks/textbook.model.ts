export interface Textbook {
  id: string;
  title: string;
  comments: string;
  imagePath: string;
  createdBy: string;
}
