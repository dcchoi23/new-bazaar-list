import { NgModule } from '@angular/core';
import { TxtbkAdCreateComponent } from './txtbk-ad-create/txtbk-ad-create.component';
import { TxtbkAdListComponent } from './txtbk-ad-list/txtbk-ad-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material.module';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        TxtbkAdCreateComponent,
        TxtbkAdListComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        AngularMaterialModule,
        RouterModule
    ]
})

export class AdsModule { }
