import { Component, OnInit, OnDestroy } from '@angular/core';
import { Textbook } from '../textbook.model';
import { TextbookService } from '../textbook.service';
import { Subscription } from 'rxjs';
import { PageEvent } from '@angular/material';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-txtbk-ad-list',
  templateUrl: './txtbk-ad-list.component.html',
  styleUrls: ['./txtbk-ad-list.component.css']
})
export class TxtbkAdListComponent implements OnInit, OnDestroy {
  textbookAds: Textbook[] = [];
  isLoading = false;
  totalNumberOfAds = 0;
  adsPerPage = 5;
  currentPage = 1;
  pageSizeOptions = [3, 5, 7, 10];
  isUserAuthenticated = false;
  userId: string;
  private adSubscription: Subscription;
  private authListenerSubscription: Subscription;

  constructor(public textbookService: TextbookService, private userService: UserService) { }

  ngOnInit() {
    this.isLoading = true;
    this.textbookService.getTextbooks(this.adsPerPage, this.currentPage);
    this.userId = this.userService.getUserId();
    this.adSubscription = this.textbookService
      .getAdUpdateListener()

      // Whenever navigating to this component, retrieve JS object containing the list of ads + total number of ads
      .subscribe((textbookAds: { posts: Textbook[], postCount: number }) => {
        this.isLoading = false;
        this.totalNumberOfAds = textbookAds.postCount;
        this.textbookAds = textbookAds.posts;
      });
      this.isUserAuthenticated = this.userService.getIsAuth();
      this.authListenerSubscription = this.userService.getAuthStatusListener().subscribe(authenticationStatus => {
        this.isUserAuthenticated = authenticationStatus;
        this.userId = this.userService.getUserId();
      });
  }

  ngOnDestroy() {
    this.adSubscription.unsubscribe();
    this.authListenerSubscription.unsubscribe();
  }

  onPageChange(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.adsPerPage = pageData.pageSize;
    this.textbookService.getTextbooks(this.adsPerPage, this.currentPage);
  }

  onDelete(postId: string) {
    this.textbookService.deletePost(postId).subscribe(() => {
      // Delete an ad and refetch the list of ads
      this.textbookService.getTextbooks(this.adsPerPage, this.currentPage);
    });
  }
}
