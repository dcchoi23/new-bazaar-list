import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Textbook } from '../textbook.model';
import { TextbookService } from '../textbook.service';
import { mimeType } from './mime-type.validator';

@Component({
  selector: 'app-txtbk-ad-create',
  templateUrl: './txtbk-ad-create.component.html',
  styleUrls: ['./txtbk-ad-create.component.css']
})
export class TxtbkAdCreateComponent implements OnInit {
  // mode controls whether application is in edit or create mode
  private mode = 'create';
  private postId: string;
  isLoading = false;
  textbook: Textbook;
  form: FormGroup;
  imagePreview: string;

  // Inject Textbook Service into this component
  constructor(
    public textbookService: TextbookService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, { validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)] }),
      comments: new FormControl(null, { validators: [Validators.required, Validators.maxLength(500)] }),
      image: new FormControl(null, { validators: [Validators.required], asyncValidators: [mimeType] })
    });

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('postId')) {
        this.mode = 'edit';
        this.postId = paramMap.get('postId');
        this.isLoading = true;
        this.textbookService.getTextbookAd(this.postId).subscribe(postData => {
          this.isLoading = false;
          this.textbook = {
            id: postData._id,
            title: postData.title,
            comments: postData.comments,
            imagePath: postData.imagePath,
            createdBy: postData.createdBy
          };

          // When editing an ad, pre-populate the form with values from the retrieval of the ad via the database
          this.form.setValue({ title: this.textbook.title, comments: this.textbook.comments, image: this.textbook.imagePath });
        });
      } else {
        this.mode = 'create';
        this.postId = null;
      }
    });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    // Put file (image) object selected into the image FormControl which is part of the FormGroup
    this.form.patchValue({ image: file });
    this.form.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  onSaveAd() {
    // If form has invalid input, don't submit to server
    if (this.form.invalid) {
      return;
    }

    this.isLoading = true;
    if (this.mode === 'create') {
      this.textbookService.submitTextbookAd(
        this.form.value.title,
        this.form.value.comments,
        this.form.value.image
      );
    } else {
      this.textbookService.updateAd(
        this.postId,
        this.form.value.title,
        this.form.value.comments,
        this.form.value.image
      );
    }

    // Clear out form input fields upon submission of form
    this.form.reset();
  }
}
