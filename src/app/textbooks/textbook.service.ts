import { Textbook } from './textbook.model';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class TextbookService {
  private textbooks: Textbook[] = [];
  private adsUpdated = new Subject<{ posts: Textbook[], postCount: number }>();
  private SERVER_URL = environment.apiUrl + '/textbookAds/';

  // Inject httpClient service
  constructor(private http: HttpClient, private router: Router) { }

  getTextbooks(postsPerPage: number, currentPage: number) {
    const params = `?pageSize=${postsPerPage}&page=${currentPage}`;

    // make http GET request. Returns JSON body consisting of message and Textbook array
    this.http
      .get<{ message: string; textbooks: any, maxPosts: number }>(
        this.SERVER_URL + params
      )
      .pipe(
        map(responseData => {
          return {
            posts: responseData.textbooks.map(post => {
              // Every element of the textbooks array (from the HTTP response object) gets converted to this type of object
              return {
                title: post.title,
                comments: post.comments,
                id: post._id,
                imagePath: post.imagePath,
                createdBy: post.createdBy
              };
            }),
            maxPosts: responseData.maxPosts
          };
        })
      )
      .subscribe(transformedPostsData => {
        this.textbooks = transformedPostsData.posts;
        this.adsUpdated.next({ posts: [...this.textbooks], postCount: transformedPostsData.maxPosts });
      });
  }

  getAdUpdateListener() {
    return this.adsUpdated.asObservable();
  }

  // fetch a single ad post, given an ID
  getTextbookAd(postId: string) {
    return this.http.get<{ _id: string, title: string, comments: string, imagePath: string, createdBy: string }>(
      this.SERVER_URL + postId
    );
  }

  submitTextbookAd(title: string, comments: string, image: File) {
    const textbookAd = new FormData();
    textbookAd.append('title', title);
    textbookAd.append('comments', comments);
    textbookAd.append('image', image, title);

    // make http POST request here, textbookAd is the JSON payload
    this.http
      .post<{ message: string, post: Textbook }>(
        this.SERVER_URL,
        textbookAd
      )
      .subscribe(() => {
        this.router.navigate(['/']);
      });
  }

  updateAd(id: string, title: string, comments: string, image: File | string) {
    let adData: Textbook | FormData;
    if (typeof (image) === 'object') {
      adData = new FormData();
      adData.append('id', id);
      adData.append('title', title);
      adData.append('comments', comments);
      adData.append('image', image);
    } else {
      adData = { id: id, title: title, comments: comments, imagePath: image, createdBy: null };
    }

    this.http.put(this.SERVER_URL + id, adData)
      .subscribe(() => {
        this.router.navigate(['/']);
      });
  }

  deletePost(postId: string) {
    return this.http.delete(this.SERVER_URL + postId);
  }
}
