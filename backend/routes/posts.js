const express = require("express");


// Middleware used to authenticate users before being allowed to perform certain API calls
const auth = require("../middleware/auth");
const extractFile = require("../middleware/file");

const PostController = require("../controllers/posts");

const router = express.Router();

// Middleware that filters only for POST requests.
router.post("", auth, extractFile, PostController.createAd);

// Update an existing ad through a PUT request 
router.put("/:id", auth, extractFile, PostController.updateAd);

// REST call to get all textbook ads
router.get('', PostController.getAds);

// Get a specific ad
router.get("/:id", PostController.getAdById);

// Update ad given an ID to the ad and userID of the creator 
router.delete("/:id", auth, PostController.deleteAd);

module.exports = router;
