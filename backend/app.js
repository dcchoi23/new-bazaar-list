const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const postRoutes = require('./routes/posts');
const userRoutes = require('./routes/user');
const User = require('./models/user');

const app = express();

mongoose.connect("mongodb+srv://admin:" + process.env.MONGO_DB_PWD + "@cluster0-pzcnr.mongodb.net/test?retryWrites=true", {
  useNewUrlParser: true
})
  .then(() => {
    console.log('connected to the database!');
  })
  .catch((err) => {
    console.log('connection failed ', err);
  })

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Ensure that requests going to images are forwarded to backend/images
app.use("/images", express.static(path.join("images")));

// Middleware to deal with CORS, incoming HTTP requests from clients
app.use((req, res, next) => {
  // No matter which domain the request is coming from, allowed to access resources
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Specify which client headers are allowed beside the default headers
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  // Specifiy which client REST methods are allowed
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

app.use('/api/textbookAds', postRoutes);
app.use('/api/user', userRoutes);

module.exports = app;
