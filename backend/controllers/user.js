const bcyrpt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../models/user");

exports.signUpUser = (req, res, next) => {
  bcyrpt.hash(req.body.password, 10)
    .then(hash => {
      const newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hash
      });

      newUser.save()
        .then(result => {
          res.status(201).json({
            message: 'User created!',
            result: result
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
    });
}

exports.loginUser = (req, res, next) => {
    let fetchedUser;
    User.findOne({
        email: req.body.email
      })
      // Use unique email identifier to search the DB for the user associated with that email inputted
      .then(user => {
        if (!user) {
          return res.status(401).json({
            message: "Auth failed"
          });
        }
        fetchedUser = user;
        // If user exists, check whether the inputted password is correct
        return bcyrpt.compare(req.body.password, user.password)
      })
      .then(result => {
        if (!result) {
          return res.status(401).json({
            message: "Auth failed"
          });
        }
  
        // Generate JSON web token 
        const token = jwt.sign({
          email: fetchedUser.email,
          userId: fetchedUser._id
        }, process.env.JWT_KEY, {
          expiresIn: "1h"
        });
        res.status(200).json({
          token: token,
          expiresIn: 3600,
          userId: fetchedUser._id
        });
      })
      .catch(err => {
        return res.status(401).json({
          message: "Auth failed"
        });
      });
  }
