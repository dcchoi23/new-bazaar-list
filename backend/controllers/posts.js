// import the Post model for the MongoDB 
const Post = require("../models/post");

exports.createAd = (req, res, next) => {
  const url = req.protocol + "://" + req.get("host");
  const textbookAd = new Post({
    title: req.body.title,
    comments: req.body.comments,
    imagePath: url + "/images/" + req.file.filename,
    createdBy: req.userData.userId
  });
  // save the JS object into mongo DB
  textbookAd.save().then(createdAd => {
    res.status(201).json({
      post: {
        id: createdAd._id,
        title: createdAd.title,
        comments: createdAd.comments,
        imagePath: createdAd.imagePath
      },
      message: 'Ad added successfully'
    });
  });
}

exports.updateAd = (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = req.protocol + "://" + req.get("host");
    imagePath = url + "/images/" + req.file.filename
  }
  const post = new Post({
    _id: req.body.id,
    title: req.body.title,
    comments: req.body.comments,
    imagePath: imagePath,
    createdBy: req.userData.userId
  });

  // Update ad given an ID to the ad and userID of the creator 
  Post.updateOne({
    _id: req.params.id,
    createdBy: req.userData.userId
  }, post).then(result => {
    console.log(result);

    if (result.n > 0) {
      // If update successful, send a 200 status code back, with the given msg
      res.status(200).json({
        message: 'Update successful'
      });
    } else {
      res.status(401).json({
        message: 'Update failed, not authorized to edit this ad'
      });
    }
  });
}

exports.getAds = (req, res, next) => {
  // Add params in HTTP request to enable pagination when retrieving ads
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const query = Post.find();
  let fetchedPosts;
  // Check that these params are present in the GET request
  if (pageSize && currentPage) {
    // Eg: pageSize = 10, currentPage = 2 then skip the first 10 documents when retrieving documents
    query.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }

  // If no pagination params (page, pageSize) passed, retrieve ALL documents from MongoDB
  query
    .then(documents => {
      fetchedPosts = documents;
      return Post.count();
    })
    .then(count => {
      // Send response back to client
      res.status(200).json({
        message: 'Textbook ads returned successfully!',
        textbooks: fetchedPosts,
        maxPosts: count
      });
    });
}

exports.getAdById = (req, res, next) => {
  Post.findById(req.params.id).then(post => {
    if (post) {
      res.status(200).json(post);
    } else {
      res.status(404).json({
        message: 'Ad not found'
      });
    }
  })
}

exports.deleteAd = (req, res, next) => {
  Post.deleteOne({
    _id: req.params.id,
    createdBy: req.userData.userId
  }).then(result => {
    if (result.n > 0) {
      // If update successful, send a 200 status code back, with the given msg
      res.status(200).json({
        message: 'Deletion of ad successful'
      });
    } else {
      res.status(401).json({
        message: 'Delete failed, not authorized to edit this ad'
      });
    }
  });
}
