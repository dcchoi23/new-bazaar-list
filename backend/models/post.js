const mongoose = require('mongoose');

const schema = mongoose.Schema({
  title: {type: String, required: true},
  comments: {type: String, required: true},
  imagePath: {type: String, required: true},
  createdBy: {type: mongoose.Schema.Types.ObjectId, ref: "User", required: true}
});

module.exports = mongoose.model('Post', schema);
